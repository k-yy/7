import { App } from "vue";
import { Button, message, Input } from "ant-design-vue";
const install = (app: App) => {
  app.use(Button).use(Input);
  app.config.globalProperties.$message = message;
};
export default {
  install,
};

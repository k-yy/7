import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router";
import "./styles/common.less";
import uiPlugin from "./plugin/ui.registry";
import baseComponent from "./plugin/baseComponent.registry";
import "./styles/theme.css";

createApp(App)
  .use(createPinia())
  .use(router)
  .use(uiPlugin)
  .use(baseComponent)
  .mount("#app");
